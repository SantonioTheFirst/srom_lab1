#include <ostream>
#include <iomanip>
#include <cmath>
#include <bits/stdc++.h>
using namespace std;
#include <vector>
#include <ctime>
class LongInteger {

  public:
    static const long long int BASIS = 2147483648;//2147483648;//1073741824;//536870912;// 16777216; //4294967296; //16777216;//65536; //4294967296; //65536; //2^16
    bool is_neg;
    vector<int> digits;
    class zero_divide: public exception {};

    //дружественные функции
    friend bool operator!=(const LongInteger&, const LongInteger&);
    friend const LongInteger operator/(LongInteger&, LongInteger&);
    friend bool operator==(const LongInteger& number1, const LongInteger& number2);
    friend bool operator<=(const LongInteger&, const LongInteger&);
    friend ostream& operator<<(ostream& ostream, const LongInteger& number);
    friend bool operator<(const LongInteger& number1, const LongInteger& number2);
    friend const LongInteger operator-(LongInteger number1, const LongInteger& number2);
    friend const LongInteger operator+(LongInteger number1, const LongInteger& number2);
    friend const LongInteger operator*(LongInteger number1, const LongInteger number2);

    LongInteger(string string){
      if(string.length() == 0){
        this->is_neg = 0;
      }
      else{
        if(string[0] == '-'){
          string = string.substr(1);
          this->is_neg = 1;
        }
        else{
          this->is_neg = 0;
        }
        for(long long i = string.length(); i > 0; i -= 31){
          if(i < 31)
            this->digits.push_back(stoi(string.substr(0, i).c_str(), nullptr, 10));
          else
            this->digits.push_back(stoi(string.substr(i - 31, 31).c_str(), nullptr, 10));
        }
        this->delete_zeros();
      }
    }

    LongInteger(){
      this->is_neg = 0;
    }

    LongInteger(signed long long number){
      if(number < 0){
        this->is_neg = 1;
        number = -number;
      }
      else this->is_neg = 0;
      do{
        this->digits.push_back(number % BASIS);
        number /= BASIS;
      }while(number != 0);
    }

    LongInteger(unsigned long long number){
      this->is_neg = 0;
      do{
        this->digits.push_back(number % BASIS);
        number /= BASIS;
      }while(number != 0);
    }

    void delete_zeros(){
      while(this->digits.size() > 1 && this->digits.back() == 0){
        this->digits.pop_back();
      }
      if(this->digits.size() == 1 && this->digits[0] == 0)this->is_neg = 0;
    }

    operator string()const{
      stringstream stringstream;
      stringstream << *this;
      return stringstream.str();
    }

    void hex_to_bin(string hex_str){
      //string s = "0xA";
      stringstream stringstream;
      stringstream << hex << hex_str;
      unsigned a;
      stringstream >> a;
      bitset<16777216> bitset(a);
      // "00000000000000000000000000001010"
      string bin_string = bitset.to_string();
      for(long long i = bin_string.length(); i > 0; i -= 31){
        if(i < 31)
          this->digits.push_back(atoi(bin_string.substr(0, i).c_str()));
        else
          this->digits.push_back(atoi(bin_string.substr(i - 31, 31).c_str()));
      }
      this->delete_zeros();
    }

    void HexToBin(string hex)
    {
      long int i = 0;
      string bits = "";
      while (hex[i]){
        switch(hex[i]){
          case '0':
            bits += "0000";
            break;
          case '1':
            bits += "0001";
            break;
          case '2':
            bits += "0010";
            break;
          case '3':
            bits += "0011";
            break;
          case '4':
            bits += "0100";
            break;
          case '5':
            bits += "0101";
            break;
          case '6':
            bits += "0110";
            break;
          case '7':
            bits += "0111";
            break;
          case '8':
            bits += "1000";
            break;
          case '9':
            bits += "1001";
            break;
          case 'A':
          case 'a':
            bits += "1010";
            break;
          case 'B':
          case 'b':
            bits += "1011";
            break;
          case 'C':
          case 'c':
            bits += "1100";
            break;
          case 'D':
          case 'd':
            bits += "1101";
            break;
          case 'E':
          case 'e':
            bits += "1110";
            break;
          case 'F':
          case 'f':
            bits += "1111";
            break;
        }
        i++;
      }
      while(bits[0] == '0'){
        bits = bits.substr(1);
      }
      //cout << "FROM HexToBin:" << endl << bits;
      this->digits.clear();
      for(long long i = bits.length(); i > 0; i -= 31){
        if(i < 31)
          this->digits.push_back(stoi(bits.substr(0, i).c_str(), nullptr, 2));
        else
          this->digits.push_back(stoi(bits.substr(i - 31, 31).c_str(), nullptr, 2));
      }
    }

    string to_bin_str(signed int number){
    //cout << endl << "FROM to_bin_str:" << endl << number << endl;
    if(number == std::numeric_limits<int>::min())
      return string("-1") + string(sizeof(int) * CHAR_BIT - 1, '0');
    string result;
    bool negative = (number < 0);
    if(negative)
      number = -number;
    do{
      result += ('0' + (number % 2));
      number = number / 2;
    }while(number > 0);
    if(negative)
      result = "-" + result;
    reverse(result.begin(), result.end());
    while(result.size() < 31)result = '0' + result;
    //cout << result;
    return result;
  }

    string getHex(){
      //cout << endl << "TEST FOR REVERSED INDEX" << endl << this->digits[0] << endl;
      string bin="";
      for(int i = (signed)this->digits.size()-1; i!=-1; i--){
        bin += to_bin_str(this->digits[i]);
      }
      //reverse(bin.begin(), bin.end());
      //cout << endl << endl << "BIN from getHex:" << endl << bin << endl;
      while(bin.size() % 4 != 0){
        bin = '0' + bin;
      }
      int begin = -4;
      do{
        begin += 4;
        if(begin + 4 >= (signed)bin.size())break;
      }while(bin.substr(begin, begin + 4) == "0000");
      string hex_str=""; //0000100011001110001001011000001010000011110001011101110000001100101001100111111100010101000110111110001011010011101111011110010111011010010000110011111110011001011010000100100111000101010100001110011000100011101011110000011010101101100101001011000000100111011010110001100011011000010100010101110000100011100010000010010100011001010101010001010101110100100011011101010111000010010010011001100011011101100000110001011100111101000011010101001001110100101110011001010110101110100101100111111100000101101011110000000011010110010000100101011101110101111110011111000110110001011011100011001101100110111110100011000101110100000100100001110011010110100001001000010010100111111111000000001100001101111101110110000010111000011100101110100010010101000101010001100111001111001001000100111011001001111001100110110111111111010101011001101010001001011111001000111111010000100011001010001010011100100000001110001110110101110100111011100100101100010000001100110110001010011111011110110001110101101010101011
      for(int i = begin; i < (signed)bin.size() - 3; i += 4){
        bitset<4> set(bin.substr(i, 4));
        stringstream ss;
        ss << hex << uppercase << set.to_ulong();
        hex_str += ss.str();
      }
      //cout << "LOL" << endl;
      while(hex_str[0] == '0'){
        hex_str = hex_str.substr(1, hex_str.size() - 1);
      }
      return hex_str;
    }


    const LongInteger operator-()const{
      LongInteger temp_copy(*this);
      temp_copy.is_neg = !temp_copy.is_neg;
      return temp_copy;
    }

    LongInteger& operator*=(const LongInteger& number){
      return *this = (*this * number);
    }

    LongInteger(unsigned int integer){
      this->digits.push_back(integer % BASIS);
      integer /= BASIS;
      if(integer != 0)this->digits.push_back(integer);
    }

    LongInteger(signed int integer){
      if(integer < 0)this->is_neg = 1;
      else this->is_neg = 0;
      this->digits.push_back(abs(integer) % BASIS);
      integer /= BASIS;
      if(integer != 0)this->digits.push_back(abs(integer));
    }

    void shift_to_right(){
      if(this->digits.size() == 0){
        this->digits.push_back(0);
        return;
      }
      this->digits.push_back(this->digits[this->digits.size() - 1]);
      for(size_t i = this->digits.size() - 2; i > 0; i--)this->digits[i] = this->digits[i - 1];
      this->digits[0] = 0;
    }

    LongInteger& operator/=(LongInteger& number){
      return *this = (*this / number);
    }

    bool is_odd()const{
      if(this->digits.size() == 0)return 0;
      return this->digits[0] & 1;
    }

    bool is_even()const{
      return !this->is_odd();
    }

    LongInteger powTest(LongInteger number){
      LongInteger A = *this;
      //LongInteger temp = A;
      for(LongInteger i(0); i < number - 1; i = i + 1){
        A *= *this;
      }
      return A;
    }

    LongInteger powT(const LongInteger& number)const{
			if(number == 0)
				return 1;
			if(number == 1)
				return *this;
			LongInteger power = number;
      LongInteger temp = power;
			LongInteger result(1);
			LongInteger x = *this;
			while (power != 0){
				if(power.is_even()){
				  x = x * x;
					power.shift_to_left(1);
				}
				else{
					result = result * x;
					x = x * x;
					temp.shift_to_left(1);
				}
			}
			return result;
		}

    short int compare(LongInteger number){
      this->delete_zeros();
      number.delete_zeros();
      short int result;
      //cout << endl << "In compare method :" << endl << *this << endl;
      LongInteger *that_number = &number;
      //cout << *that_number << endl;
      //cout << "a digits: " << this->digits.size() << endl << "b digits: " << that_number->digits.size() << endl;
      if(*this < *that_number)result = -1;
      if(*this == *that_number)result = 0;
      if(*this != *that_number && !(*this < *that_number))result = 1;
      return result;
    }

    int bitLength(){
      this->delete_zeros();
      if(this->digits.size()!=0){
        int bitLenghtOfLastCluster = (int) floor(log(this->digits[this->digits.size() - 1]) / log(2)) + 1;
        return (this->digits.size() - 1) * 31 + bitLenghtOfLastCluster;
      }else{
        return 0;
      }
    }

    LongInteger shift_to_left(int k){
      if(k<0)return *this;
      LongInteger a = *this;
      for(int i=0;i<k;i++){
          a = a * 2;
      }
      a.delete_zeros();
      return a;
    }

    LongInteger square(){
      return *this * (*this);
    }

    LongInteger pow(LongInteger rightOperand){
      LongInteger A = *this;
      LongInteger C(1);
      string bin;
      for(unsigned int i = rightOperand.digits.size()-1; i >= 0 ; i--){
        bin += to_bin_str(rightOperand.digits[i]);
      }

      bin = bin.substr(bin.size() - rightOperand.bitLength(), rightOperand.bitLength());

      for(unsigned int i = 0; i < bin.size(); i++){
        C = C.square();
        if(bin[i] == '1'){
          C = C * A;
        }
      }
      return C;
    }


    LongInteger subMod(LongInteger number, LongInteger mod){
        LongInteger A = (*this - number);
        LongInteger result = A.divMod(mod);
        if(this->compare(number) == 1){
          return result;
        }
        else{
          return mod - result;
        }
    }

    LongInteger divMod(LongInteger number){
      int length = number.bitLength();
      LongInteger B = number;
      LongInteger R = *this;
      int temp;
      LongInteger C;
      LongInteger ONE(1);
      int flag = R.compare(B);
      while(flag == 1 || flag == 0){
        temp = R.bitLength();
        C = B.shift_to_left(temp-length);
        if(R.compare(C) == -1){
          temp--;
          C = B.shift_to_left(temp-length);
        }
        R = R - C;
        flag = R.compare(B);
      }
      return R;
    }

    LongInteger addMod(const LongInteger& number, const LongInteger& mod){
      LongInteger A = *this + number;
      return A.divMod(mod);
    }


    LongInteger mulMod(const LongInteger& number, const LongInteger& mod){
      LongInteger A = *this * number;
      return A.divMod(mod);
    }

    LongInteger squareMod(const LongInteger& mod){
      return this->mulMod(*this, mod);
    }

    LongInteger min(LongInteger number){
      if(this->compare(number)==-1){
        return *this;
      }
      else {
        return number;
      }
    }

    LongInteger gcd(LongInteger number){
      LongInteger A = *this;
      LongInteger B = number;
      LongInteger D(1);
      LongInteger two(2);
      LongInteger zero(0);
      while (A.is_even() && B.is_even()){
        A = A/two;
        B = B/two;
        D = D * two;
      }
      while(A.is_even()){
        A = A/two;
      }
      while(B.compare(zero) != 0){
        while(B.is_even()){
          B = B/two;
        }
        LongInteger temp = A;
        A = A.min(B);
        B = B - temp;
      }
      return D * A;
    }

    LongInteger lcm(LongInteger number){
      LongInteger A = *this * number;
      LongInteger B = this->gcd(number);
      //return (*this * number) / (this->gcd(number));
      return A / B;
    }


};

bool operator==(const LongInteger& number1, const LongInteger& number2){
  if(number1.is_neg != number2.is_neg)return 0;
  if(number1.digits.empty()){
    if((number2.digits[0] == 0 && number2.digits.size() == 1) || number2.digits.empty())return 1;
    else return 0;
  }
  if(number2.digits.empty()){
    if(number1.digits.size() == 1 && number1.digits[0] == 0)return 1;
    else return 0;
  }
  // одинаковое кол-во цифр
  if(number1.digits.size() != number2.digits.size())return 0;
  for(size_t i = 0; i < number1.digits.size(); i++){
    if(number1.digits[i] != number2.digits[i])return 0;
  }
  return 1;
}

const LongInteger operator*(LongInteger number1, const LongInteger number2){
  LongInteger res;
  bool pos;
  if(!number1.is_neg && !number2.is_neg)pos = 1;
  res.digits.resize(number1.digits.size() + number2.digits.size());
  for(size_t i = 0; i < number1.digits.size(); i++){
    int carry = 0;
    for(size_t j = 0; j < number2.digits.size() || carry != 0; j++){
      long long current = res.digits[i + j] + number1.digits[i] * 1LL * (j < number2.digits.size() ? number2.digits[j] : 0) + carry;
      res.digits[i + j] = static_cast<int>(current % LongInteger::BASIS);
      if(res.digits[i + j] < 0){
        do{
          res.digits[i + j] = res.digits[i + j] + LongInteger::BASIS;
        }while(res.digits[i + j] < 0 && pos == true);
      }
      carry = static_cast<int>(current / LongInteger::BASIS);
    }
  }
  res.is_neg = number1.is_neg != number2.is_neg;
  res.delete_zeros();
  return res;
}

ostream& operator<<(ostream& ostream, const LongInteger& number){
  if(number.digits.empty())ostream << 0;
  else{
    if(number.is_neg) ostream << '-';
    ostream << number.digits.back();
    //сохраняем данный символ заполнитель, тк след. числа надо выводить по 31 шт
    char old_filler = ostream.fill('0');
    for(long long i = static_cast<long long>(number.digits.size()) - 2; i >= 0; i--){
      ostream << setw(31) << number.digits[i];
    }
    ostream.fill(old_filler);
  }
  return ostream;
}

bool operator!=(const LongInteger& number1, const LongInteger& number2){
  return !(number1 == number2);
}

const LongInteger operator-(LongInteger number1, const LongInteger& number2){
  bool pos;
  if(number2.is_neg)return number1 + (-number2);
  else if(number1.is_neg)return -(-number1 + number2);
  else if(number1 < number2)return -(number2 - number1);
  else if(!number1.is_neg && !number2.is_neg)pos = 1;
  int carry = 0;
  for(size_t i = 0; i < number2.digits.size() || carry != 0; i++){
    number1.digits[i] -= carry + (i < number2.digits.size() ? number2.digits[i] : 0);
    carry = number1.digits[i] < 0;
    if(carry != 0) number1.digits[i] += LongInteger::BASIS;
    if(number1.digits[i] < 0){
      do{
        number1.digits[i] = number1.digits[i] + LongInteger::BASIS;
      }while(number1.digits[i] < 0 && pos == 1);
    }
  }
  number1.delete_zeros();
  return number1;
}

const LongInteger operator+(LongInteger number1, const LongInteger& number2){
  if(number1.is_neg){
    if(number2.is_neg)return -(-number1 + (-number2));
    else return number2 - (-number1);
  }
  else if(number2.is_neg)return number1 - (-number2);
  int carry = 0;
  for(size_t i = 0; i < max(number2.digits.size(), number1.digits.size()) || carry != 0; i++){
    bool pos;
    if(number1.digits[i] > 0 && number2.digits[i] > 0) pos = true;
    if(i == number1.digits.size())number1.digits.push_back(0);
    number1.digits[i] += carry + (i < number2.digits.size() ? number2.digits[i] : 0);
    carry = number1.digits[i] >= LongInteger::BASIS;
    if(carry != 0)number1.digits[i] -= LongInteger::BASIS;
    if(number1.digits[i] < 0){
      do{
        number1.digits[i] = number1.digits[i] + LongInteger::BASIS;
      }while(number1.digits[i] < 0 && pos == true);
    }
  }
  return number1;
}

bool operator<(const LongInteger& number1, const LongInteger& number2){
  if(number1 == number2)return 0;
  if(number1.is_neg){
    if(number2.is_neg)return ((-number2) < (-number1));
    else return 1;
  }
  else if(number2.is_neg)return 0;
  else{
    if(number1.digits.size() != number2.digits.size()){
      return number1.digits.size() < number2.digits.size();
    }
    else{
    for(long long i = number1.digits.size() - 1; i >= 0; i--){
      if(number1.digits[i] != number2.digits[i])return number1.digits[i] < number2.digits[i];
    }
    return 0;
    }
  }
}

const LongInteger operator/(LongInteger &number1, LongInteger &number2){
  if(number2 == 0)throw LongInteger::zero_divide();
  //clock_t start = clock();
  int length = number2.bitLength();
  //cout << "bitLength = " << length << endl;
  LongInteger ZERO(0);
  int temp;
  LongInteger C;
  LongInteger ONE(1);
  int flag = number1.compare(number2);
  //cout << "STATEMENT " << statement << endl;
  while(flag == 1 || flag == 0){
    temp = number1.bitLength();
    C = number2.shift_to_left(temp - length);
    //cout << "SHIFTED LEFT" << endl;
    if(number1.compare(C) == -1){
      temp--;
      C=number2.shift_to_left(temp - length);
    }
    number1 = number1 - C;
    ZERO = ZERO + ONE.shift_to_left(temp - length);
    flag = number1.compare(number2);
  }
    //cout << endl << "TIME DIVIDE" << endl << (double) (clock() - start) / CLOCKS_PER_SEC;
    return ZERO;
}

bool operator<=(const LongInteger& number1, const LongInteger& number2){
  return(number1 == number2 || number1 < number2);
}
